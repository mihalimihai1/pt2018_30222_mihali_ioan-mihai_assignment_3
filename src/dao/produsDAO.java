package dao;
import java.sql.*;
import java.util.ArrayList;

import javax.swing.JTable;

import connection.Conexiune;
import model.produs;

public class produsDAO {
	private static final String findStatementString = "SELECT * FROM alcool2.produs;";
	ResultSet reset = null;

	private int count() {
		Connection ConBd = Conexiune.getConnection();
		ResultSet rs = null;
		int val=0;
		Statement statement = null;

		try {
			statement = ConBd.createStatement();
			rs = statement.executeQuery("SELECT count(*) FROM alcool2.produs");
			while (rs.next()) {
				val = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;

	}

	public JTable populatefirst() {
		Object[][] obiecte = new Object[count()][4];
		int nrinregistrari = 0;
		int all = 0;
		String[] coloane = { "idprodus", "denumire", "cantitate", "pret" };
		Connection ConBd = Conexiune.getConnection();
		ResultSet reset = null;
		PreparedStatement findStatement = null;
		try {
			findStatement = ConBd.prepareStatement(findStatementString);
			reset = findStatement.executeQuery();

			while (reset.next()) {
				System.out.println(reset.getInt("idprodus"));
				obiecte[nrinregistrari][0] = reset.getInt("idprodus");
				obiecte[nrinregistrari][1] = reset.getString("denumire");
				obiecte[nrinregistrari][2] = reset.getString("cantitate");
				obiecte[nrinregistrari][3] = reset.getString("pret");
				nrinregistrari++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(reset);
			Conexiune.close(findStatement);
		}

		return new JTable(obiecte, coloane);

	}

	public ArrayList<produs> listaClienti = new ArrayList<produs>();
	private static final String insertStatementString = "INSERT INTO `alcool2`.`produs` (`idprodus`, `denumire`, `cantitate`, `pret`) VALUES (?, ?, ?, ?);\n";

	public void inserare(produs client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			insertStatement = ConBd.prepareStatement(insertStatementString);
			findStatement =ConBd.prepareStatement(findStatementString);
			insertStatement.setInt(1, client.getIdprodus());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getCantitate());
			insertStatement.setString(4, client.getPret());
			insertStatement.executeUpdate();
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new produs(rs.getInt("idprodus"), rs.getString("denumire"), rs.getString("cantitate"),rs.getString("pret")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally
		{
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(insertStatement);
			
		}
	}

	private static final String deleteStatementString = "DELETE FROM `alcool2`.`produs` WHERE idprodus=?;";

	public void delete(produs client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement deleteStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			deleteStatement = ConBd.prepareStatement(deleteStatementString);
			findStatement =ConBd.prepareStatement(findStatementString);
			deleteStatement.setInt(1, client.getIdprodus());
			deleteStatement.executeUpdate();
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new produs(rs.getInt("idprodus"), rs.getString("denumire"), rs.getString("cantitate"), rs.getString("pret")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(deleteStatement);
			
		}
	}

	public void updatelista(produs client) {
		for (int i = 0; i < listaClienti.size(); i++) {
			if (listaClienti.get(i).getIdprodus() == client.getIdprodus()) {
				listaClienti.get(i).setNume(client.getNume());
				listaClienti.get(i).setCantitate(client.getCantitate());
				listaClienti.get(i).setPret(client.getPret());
			}
		}
	}

	private static final String updateStatementString = "UPDATE alcool2.produs SET denumire=?, cantitate=? , pret=? WHERE idprodus=?;";

	public void update(produs client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement updateStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			updateStatement = ConBd.prepareStatement(updateStatementString);
			findStatement = ConBd.prepareStatement(findStatementString);
			updateStatement.setInt(4, client.getIdprodus());
			updateStatement.setString(1, client.getNume());
			updateStatement.setString(2, client.getCantitate());
			updateStatement.setString(3, client.getPret());
			updateStatement.executeUpdate();
			updatelista(client);
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new produs(rs.getInt("idprodus"), rs.getString("denumire"), rs.getString("cantitate"),rs.getString("pret")));
		} catch (SQLException e) {

		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(updateStatement);
		}
	}

}
