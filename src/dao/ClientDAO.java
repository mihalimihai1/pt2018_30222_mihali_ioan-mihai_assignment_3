package dao;
import java.sql.*;
import java.util.*;

import javax.swing.JTable;

import connection.Conexiune;
import model.Client;

public class ClientDAO {

	private static final String findStatementString = "SELECT * FROM alcool2.client;";
	ResultSet reset = null;

	private int count() {
		Connection ConBd = Conexiune.getConnection();
		ResultSet rs = null;
		int val = 0;
		Statement statement = null;

		try {
			statement = ConBd.createStatement();
			rs = statement.executeQuery("SELECT count(*) FROM alcool2.client");
			while (rs.next()) {
				val = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;

	}

	public JTable populatefirst() {
		Object[][] obiecte = new Object[count()][4];
		int nrinregistrari = 0;
		int all = 0;
		String[] coloane = { "id", "nume", "email", "telefon" };
		Connection ConBd = Conexiune.getConnection();
		ResultSet reset = null;
		PreparedStatement findStatement = null;
		try {
			findStatement = ConBd.prepareStatement(findStatementString);
			reset = findStatement.executeQuery();

			while (reset.next()) {
				System.out.println(reset.getInt("idclient"));
				obiecte[nrinregistrari][0] = reset.getInt("idclient");
				obiecte[nrinregistrari][1] = reset.getString("nume");
				obiecte[nrinregistrari][2] = reset.getString("email");
				obiecte[nrinregistrari][3] = reset.getString("telefon");
				nrinregistrari++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(reset);
			Conexiune.close(findStatement);
		}

		return new JTable(obiecte, coloane);

	}

	public ArrayList<Client> listaClienti = new ArrayList<Client>();
	private static final String insertStatementString = "INSERT INTO `alcool2`.`client` (`idclient`, `nume`, `email`, `telefon`) VALUES (?, ?, ?, ?);\n";

	public void inserare(Client client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			insertStatement = ConBd.prepareStatement(insertStatementString);
			findStatement = ConBd.prepareStatement(findStatementString);
			insertStatement.setInt(1, client.getIdClient());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getEmail());
			insertStatement.setString(4, client.getTelefon());
			insertStatement.executeUpdate();
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new Client(rs.getInt("idclient"), rs.getString("nume"), rs.getString("email"),
						rs.getString("telefon")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(insertStatement);

		}
	}

	private static final String deleteStatementString = "DELETE FROM `alcool2`.`client` WHERE idclient=?;";

	public void delete(Client client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement deleteStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			deleteStatement = ConBd.prepareStatement(deleteStatementString);
			findStatement = ConBd.prepareStatement(findStatementString);
			deleteStatement.setInt(1, client.getIdClient());
			deleteStatement.executeUpdate();
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new Client(rs.getInt("idclient"), rs.getString("nume"), rs.getString("email"),
						rs.getString("telefon")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(deleteStatement);
		}
	}

	public void updatelista(Client client) {
		for (int i = 0; i < listaClienti.size(); i++) {
			if (listaClienti.get(i).getIdClient() == client.getIdClient()) {
				listaClienti.get(i).setEmail(client.getEmail());
				listaClienti.get(i).setNume(client.getNume());
				listaClienti.get(i).setTelefon(client.getTelefon());
			}
		}
	}

	private static final String updateStatementString = "UPDATE alcool2.client SET nume=?,email=? ,telefon=? WHERE idclient=?;";

	public void update(Client client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement updateStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;

		try {
			updateStatement = ConBd.prepareStatement(updateStatementString);
			findStatement = ConBd.prepareStatement(findStatementString);
			updateStatement.setInt(4, client.getIdClient());
			updateStatement.setString(1, client.getNume());
			updateStatement.setString(2, client.getEmail());
			updateStatement.setString(3, client.getTelefon());
			updateStatement.executeUpdate();
			updatelista(client);
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new Client(rs.getInt("idclient"), rs.getString("nume"), rs.getString("email"),
						rs.getString("telefon")));
		} catch (SQLException e) {

		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
			Conexiune.close(updateStatement);
		}
	}

}
