package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import dao.*;
import model.comanda;

public class GUI {
	
	public JButton b1;
	private JButton b2;
	private JPanel panou, panou2;
	private JLabel l1,l2,l3,l4, labeltext, pretlabel;
	public JTextField t1;
	private JTextField t2;
	public JTextField t3;
	private JTextField t4;
	private JTextField idtext;
	private JTextField prettext;
	JFrame frame = new JFrame("Comenzile");
	private ArrayList<comanda> com = new ArrayList<>();
	private JTable table = new JTable();
    private DefaultTableModel mod = (DefaultTableModel) table.getModel();
    comandaDAO daoC = new comandaDAO();

    
    comanda c=null;
    
	public GUI() {
	
	
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setLayout(new GridLayout(1,2));
	frame.setSize(600, 300);
	frame.setBackground(Color.YELLOW);
	frame.setForeground(Color.YELLOW);
	panou = new JPanel();
	panou2 = new JPanel();
	panou.setLayout(null);
	panou.setBackground(Color.GREEN);
	
	
	b1=new JButton("Adauga comanda");
	
	
	labeltext = new JLabel("IDorder:");
	l1=new JLabel("Numeprodus:");
	l2=new JLabel("IDclient:");
	l3=new JLabel("Cantitate:");
	l4=new JLabel("Pret:");
	
	frame.setBounds(150,100,800,300);
	labeltext.setBounds(10,10,100,18);
	panou.add(labeltext);
	l1.setBounds(10,30,100,18); //x y lungime latime
	panou.add(l1);
	l2.setBounds(10,50,100,18);
	panou.add(l2);
	l3.setBounds(10,70,100,18);
	panou.add(l3);
	
	idtext=new JTextField("");
	idtext.setBounds(90,10,150,18);
	panou.add(idtext);
	
	t1=new JTextField("");
	t1.setBounds(90,30,150,18);
	panou.add(t1);
	
	t2=new JTextField("");
	t2.setBounds(90,50,150,18);
	panou.add(t2);
	
	t3=new JTextField("");
	t3.setBounds(90,70,150,18);
	panou.add(t3);
	
	
	b1.setBounds(10, 130, 230, 30);
	panou.add(b1);
	/////////////////
	l4.setBounds(10,90,150,18);
	panou.add(l4);
	t4=new JTextField("");
	t4.setBounds(90,90,150,18);
	panou.add(t4);
	
	
	frame.add(panou);
	frame.add(panou2);
	////////////////////
	panou2.add(daoC.populatefirst());
	
	b1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {

		
			daoC.inserare(new comanda(Integer.parseInt(idtext.getText()), t1.getText(), Integer.parseInt(t2.getText()),Integer.parseInt(t3.getText()), Integer.parseInt(t4.getText()),0));
			repopulateTable();
				}
	});
    ////////////////
    frame.setVisible(true);
	}
	
	private void repopulateTable()
	 {
			//panou2.removeAll(); // ii dam remove la tabelul vechi
			frame.remove(panou2);
			frame.add(table);
			 // punem tabelul nou
			table.setModel(mod); //punem modelul in tabel
			mod.setRowCount(0); //curatam tabelul
			mod.setColumnCount(0);
			 

			Object[] obiecte = new Object[6];
			
			mod.addColumn("idorder");
			mod.addColumn("numeprodus");
			mod.addColumn("idclient");
			mod.addColumn("cantitate");
			mod.addColumn("pret");
			mod.addColumn("total");
			
			System.out.println(daoC.listaClienti + "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
			mod.setRowCount(0);
			for (int i = 0; i < daoC.listaClienti.size(); i++) {
				obiecte[0] = daoC.listaClienti.get(i).getIdorder();
				obiecte[1] = daoC.listaClienti.get(i).getNumeProdus();
				obiecte[2] = daoC.listaClienti.get(i).getIdClient();
				obiecte[3] = daoC.listaClienti.get(i).getCantitate();
			    obiecte[4] = daoC.listaClienti.get(i).getPret();
			    obiecte[5] = daoC.listaClienti.get(i).getTotal();
				mod.insertRow(i,obiecte);  //adaugam cate un rand   
				System.out.println(obiecte[0]);
			}
			
			
	 }

}
