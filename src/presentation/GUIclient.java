package presentation;
import java.awt.BorderLayout;
import dao.*;
import model.Client;
import model.comanda;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GUIclient {
	private JButton b1, b2, b3;
	private JPanel panou, panou2;
	private JLabel l1, l2, l3, l4;
	private JTextField t1, t2, t3, t4;
	JFrame frame = new JFrame("Clientii");
	private ArrayList<Client> client = new ArrayList<>();
	public JTable table = new JTable();
	DefaultTableModel mod = (DefaultTableModel) table.getModel();
	ClientDAO daoC = new ClientDAO();

	comanda c = null;

	public GUIclient() {

		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(1, 2));
		frame.setSize(600, 300);
		frame.setBackground(Color.YELLOW);
		frame.setForeground(Color.YELLOW);

		panou = new JPanel();
		panou.setLayout(null);
		panou.setBackground(Color.YELLOW);
		panou2 = new JPanel();

		b1 = new JButton("Add");
		b2 = new JButton("Delete");
		b3 = new JButton("Update");

		l1 = new JLabel("IDclient:");
		l2 = new JLabel("Nume:");
		l3 = new JLabel("Email:");
		l4 = new JLabel("Telefon:");

		frame.setBounds(150, 100, 650, 300);

		l1.setBounds(10, 10, 100, 18); // x y lungime latime
		panou.add(l1);
		l2.setBounds(10, 30, 100, 18);
		panou.add(l2);
		l3.setBounds(10, 50, 100, 18);
		panou.add(l3);

		t1 = new JTextField("");
		t1.setBounds(90, 10, 150, 18);
		panou.add(t1);
		t2 = new JTextField("");
		t2.setBounds(90, 30, 150, 18);
		panou.add(t2);
		t3 = new JTextField("");
		t3.setBounds(90, 50, 150, 18);
		panou.add(t3);

		b1.setBounds(10, 90, 100, 40);
		panou.add(b1);
		/////////////////
		l4.setBounds(10, 70, 150, 18);
		panou.add(l4);
		t4 = new JTextField("");
		t4.setBounds(90, 70, 150, 18);
		panou.add(t4);
		b2.setBounds(115, 90, 100, 40);
		panou.add(b2);
		b3.setBounds(10, 140, 100, 40);
		panou.add(b3);

		////////////////////

		frame.add(panou);
		frame.add(panou2);

		panou2.add(daoC.populatefirst());
		////////////////

		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				daoC.inserare(new Client(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable();
			}
		});
		
		b2.addActionListener(new ActionListener ( ) {
			public void actionPerformed(ActionEvent e)
			{
				daoC.delete(new Client(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable();
			}
		});
		
		b3.addActionListener(new ActionListener ( ) {
			public void actionPerformed(ActionEvent e)
			{
				daoC.update(new Client(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable();
			}
		});

		frame.setVisible(true);
	}

	 private void repopulateTable()
	 {
			//panou2.removeAll(); // ii dam remove la tabelul vechi
			frame.remove(panou2);
			frame.add(table); // punem tabelul nou
			table.setModel(mod); //punem modelul in tabel
			mod.setRowCount(0); //curatam tabelul
			mod.setColumnCount(0);
			 
           
			Object[] obiecte = new Object[4];
			
			mod.addColumn("id");
			mod.addColumn("nume");
			mod.addColumn("email");
			mod.addColumn("telefon");
			
			
			System.out.println(daoC.listaClienti + "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
			mod.setRowCount(0);
			for (int i = 0; i < daoC.listaClienti.size(); i++) {
				obiecte[0] = daoC.listaClienti.get(i).getIdClient();
				obiecte[1] = daoC.listaClienti.get(i).getNume();
				obiecte[2] = daoC.listaClienti.get(i).getEmail();
			    obiecte[3] = daoC.listaClienti.get(i).getTelefon();
				mod.insertRow(i,obiecte);  //adaugam cate un rand   
				System.out.println(obiecte[0]);
			}
			
			
	 }

}
