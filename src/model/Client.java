package model;
public class Client {
	public Client(int idClient, String nume, String email, String telefon) {
		this.idClient = idClient;
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
	}
	private int idClient;
    private String nume;
    private String email;
    private String telefon;
    
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nume=" + nume + ", email=" + email + ", telefon=" + telefon + "]";
	}
	
}
