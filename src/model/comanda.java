package model;
public class comanda {
	public comanda(int idorder, String numeProdus, int idClient, int cantitate,int pret, double total) {
		super();
		this.idorder = idorder;
		this.numeProdus = numeProdus;
		this.idClient = idClient;
		this.cantitate = cantitate;
		this.pret=pret;
		this.total = total;
	}
	private int idorder;
    private String numeProdus;
    private int idClient;
    private int cantitate;
    private int pret;
    private double total;
    
	public int getIdorder() {
		return idorder;
	}
	public void setIdorder(int idorder) {
		this.idorder = idorder;
	}
	public String getNumeProdus() {
		return numeProdus;
	}
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret=pret;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "comanda [idorder=" + idorder + ", numeProdus=" + numeProdus + ", idClient=" + idClient + ", cantitate="
				+ cantitate + ", total=" + total + "]";
	}
}
